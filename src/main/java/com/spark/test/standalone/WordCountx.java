package com.spark.test.standalone;

import java.io.Serializable;

public class WordCountx implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1903249083213788691L;
	private int counts;
	private String word;
	
	public WordCountx(int counts, String word) {
		this.counts = counts;
		this.word = word;
	}
	
	public int getCounts() {
		return counts;
	}
	public void setCounts(int counts) {
		this.counts = counts;
	}
	public String getWord() {
		return word;
	}
	public void setWord(String word) {
		this.word = word;
	}
	
	
}
