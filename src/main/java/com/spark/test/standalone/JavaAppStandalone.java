package com.spark.test.standalone;

import java.io.Serializable;
import java.util.Arrays;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;

import scala.Tuple2;

public class JavaAppStandalone implements Serializable {
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8214565893370341460L;


	public static void main(String[] args){
		
		JavaAppStandalone app = new JavaAppStandalone();
		app.init(args[0], args[1]);
	}
	
	
	public void init(String filePath, String outputFile){
		
		SparkConf conf = new SparkConf().setAppName("standalone")
				.setMaster("local");
		JavaSparkContext js = new JavaSparkContext(conf);
		
		JavaRDD<String> input = js.textFile(filePath);
		
		JavaRDD<String> words = input.flatMap(
				  new FlatMapFunction<String, String>() {
				    public Iterable<String> call(String x) {
				      return Arrays.asList(x.split(" "));
		}});
		
		JavaPairRDD<String, Integer> counts = words.mapToPair(
				new PairFunction<String, String, Integer>(){
				    @SuppressWarnings("unchecked")
					public Tuple2<String, Integer> call(String x){
				    		return new Tuple2(x, 1);
				    }}).reduceByKey(
				    		new Function2<Integer, Integer, Integer>(){
				        public Integer call(Integer x, Integer y)
				        { 
				        	return x + y;}
				        }
				    );
		
		
		counts.saveAsTextFile(outputFile);
		
		
	}
	
}
