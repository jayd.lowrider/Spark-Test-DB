package com.spark.test.standalone;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import kafka.consumer.ConsumerConfig;
import kafka.consumer.ConsumerIterator;
import kafka.consumer.KafkaStream;


public class KafkaOnlyTest {
	
	
	public static void main(String[] args){
		KafkaOnlyTest tp  = new KafkaOnlyTest();
		//tp.doConsumerTest();
		tp.doProduceTest();
		
	}
	public KafkaOnlyTest(){
		
	}

	public void doConsumerTest(){
		
		new Thread(
			
			new Runnable() {
				
				private ConsumerConfig createConsumerConfig(String a_zookeeper, String a_groupId) {
				        Properties props = new Properties();
				        props.put("zookeeper.connect", a_zookeeper);
				        props.put("group.id", a_groupId);
				        props.put("zookeeper.session.timeout.ms", "400");
				        props.put("zookeeper.sync.time.ms", "200");
				        props.put("auto.commit.interval.ms", "1000");
				 
				        return new ConsumerConfig(props);
				}
				@Override
				public void run() {
					
					kafka.javaapi.consumer.ConsumerConnector consumer = kafka.consumer.Consumer.createJavaConsumerConnector(
				                createConsumerConfig("localhost:2181", "somegroup"));
					
					    Map<String, Integer> topicCountMap = new HashMap<String, Integer>();
				        topicCountMap.put("topic-word-count", new Integer(1));
				        Map<String, List<KafkaStream<byte[], byte[]>>> consumerMap = consumer.createMessageStreams(topicCountMap);
				        List<KafkaStream<byte[], byte[]>> streams = consumerMap.get("topic-word-count");
				 
				        // now launch all the threads
				        //
				        ExecutorService executor = Executors.newFixedThreadPool(10);
				        
				        // now create an object to consume the messages
				        //
				        int threadNumber = 0;
				        for (final KafkaStream stream : streams) {
				            executor.submit(new ConsumerTest(stream, threadNumber));
				            threadNumber++;
				        }
					
				};
			}	
		).start();
	}
	
	
	public static class ConsumerTest implements Runnable {
	    private KafkaStream m_stream;
	    private int m_threadNumber;
	 
	    public ConsumerTest(KafkaStream a_stream, int a_threadNumber) {
	        m_threadNumber = a_threadNumber;
	        m_stream = a_stream;
	    }
	 
	    public void run() {
	        ConsumerIterator<byte[], byte[]> it = m_stream.iterator();
	        while (it.hasNext())
	            System.out.println("Thread " + m_threadNumber + ": " + new String(it.next().message()));
	        System.out.println("Shutting down Thread: " + m_threadNumber);
	    }
	}
	
	public void doProduceTest(){
		
		 Properties props = new Properties();
		 props.put("metadata.broker.list", "localhost:9092");
		 props.put("bootstrap.servers", "localhost:9092");
		 props.put("serializer.class", "org.apache.kafka.common.serialization.StringSerializer");
		 props.put("key.serializer","org.apache.kafka.common.serialization.StringSerializer" );
		 props.put("value.serializer","org.apache.kafka.common.serialization.StringSerializer" );
		 
		 
		 //props.put("partitioner.class", "example.producer.SimplePartitioner");
		 props.put("request.required.acks", "1");

		 
		 KafkaProducer producer = new KafkaProducer(props);
		 for(int i = 0; i < 100; i++)
		     producer.send(new ProducerRecord<String, String>("topic-word-count", "Key", "This is freaking awesome"));

		 producer.close();
		
	}

}
