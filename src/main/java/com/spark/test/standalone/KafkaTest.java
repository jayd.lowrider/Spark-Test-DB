package com.spark.test.standalone;

import java.sql.PreparedStatement;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.api.java.function.VoidFunction2;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.types.StructType;
import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.Time;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.apache.spark.streaming.api.java.JavaPairInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka.KafkaUtils;

import com.google.common.collect.Lists;

import kafka.serializer.StringDecoder;
import scala.Tuple2;

public class KafkaTest {
	
	private static final Pattern SPACE = Pattern.compile(" ");

	private KafkaTest(){}
	
	public static void main(String[] args) {
		if (args.length < 2) {
			System.err.println("Usage: JavaKafkaWordCount <brokers> <topics> ");
			System.exit(1);
		}

//		StreamingExamples.setStreamingLogLevels();
		SparkConf sparkConf = new SparkConf().setAppName("JavaKafkaWordCount").setMaster("local[2]");
		//		
//		System.out.println("-------------Attach debugger now!--------------");
//		try {
//			Thread.sleep(8000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		sparkConf.set("url", "jdbc:postgresql://localhost:32768/postgres");
		sparkConf.set("dbtable", "public.testcounts");
		sparkConf.set("driver", "org.postgresql.Driver");
		//sparkConf.set("spark.sql.dialect", "sql");
		
		// Create the context with 5 minutes batch size
		JavaStreamingContext jssc = new JavaStreamingContext(sparkConf, new Duration(60000));
		jssc.sparkContext().addJar("/home/mramos/.gradle/caches/modules-2/files-2.1/postgresql/postgresql/9.1-901-1.jdbc4/9bfabe48876ec38f6cbaa6931bad05c64a9ea942/postgresql-9.1-901-1.jdbc4.jar");

		jssc.checkpoint("/tmp/spark/");
		
		String brokers = args[0];
	    String topics = args[1];
		
		Set<String> topicsSet = new HashSet<String>(Arrays.asList(topics.split(",")));
	    HashMap<String, String> kafkaParams = new HashMap<String, String>();
	    kafkaParams.put("metadata.broker.list", brokers);

	    // Create direct kafka stream with brokers and topics
	    JavaPairInputDStream<String, String> messages = KafkaUtils.createDirectStream(
	        jssc,
	        String.class,
	        String.class,
	        StringDecoder.class,
	        StringDecoder.class,
	        kafkaParams,
	        topicsSet
	    );
	    
		JavaDStream<String> lines = messages.map(new Function<Tuple2<String, String>, String>() {
			@Override
			public String call(Tuple2<String, String> tuple2) {
				return tuple2._2();
			}
		});

		JavaDStream<String> words = lines.flatMap(new FlatMapFunction<String, String>() {
			@Override
			public Iterable<String> call(String x) {
				return Lists.newArrayList(SPACE.split(x));
			}
		});

		JavaPairDStream<String, Integer> wordCounts = words.mapToPair(new PairFunction<String, String, Integer>() {
			@Override
			public Tuple2<String, Integer> call(String s) {
				return new Tuple2<String, Integer>(s, 1);
			}
		}).reduceByKey(new Function2<Integer, Integer, Integer>() {
			@Override
			public Integer call(Integer i1, Integer i2) {
				return i1 + i2;
			}
		});
		
		
		//create the SQL Context
		//SQLContext sqlContext = new org.apache.spark.sql.SQLContext( jssc.sparkContext().sc()  );
		
//		wordCounts.foreachRDD(new Function2<JavaPairRDD<String, Integer>, Time, Void> (){
//			
//			@Override
//			public Void call(final JavaPairRDD<String, Integer> v1, Time v2) throws Exception {
//				SQLContext sqlContext = JavaSQLContextSingleton.getInstance(v1.context());
//				
//				JavaRDD<TestCounts> rowRDD = v1.map(new Function<Tuple2<String,Integer>, TestCounts>() {
//
//					@Override
//					public TestCounts call(Tuple2<String, Integer> v1) throws Exception {
//						TestCounts tC = new TestCounts(v1._2(), v1._1());
//			            return tC;
//					}
//					
//				});
//				
//				org.apache.spark.sql.DataFrame wordsDataFrame = 
//						 sqlContext.createDataFrame(rowRDD, TestCounts.class);
//				
//				wordsDataFrame.registerTempTable("words");
//				
//				 // Do word count on table using SQL and print it
//			      DataFrame wordCountsDataFrame =
//			          sqlContext.sql("select word, count(*) as total from words group by word");
//			      wordCountsDataFrame.show();
//			      
//				return null;
//			}
//		});
//		
		wordCounts.foreachRDD(new VoidFunction2<JavaPairRDD<String, Integer>, Time>() {

			@Override
			public void call(JavaPairRDD<String, Integer> v1, Time v2) throws Exception {
				
				Properties options = new Properties();
//				Map<String, String> options = new HashMap<>();
				options.put("url", "jdbc:postgresql://localhost:32768/postgres");
//				options.put("dbtable", "wordcountx");
				options.put("user", "postgres");
				options.put("password", "postgres");
				options.put("driver", "org.postgresql.Driver");

				SQLContext sqlContext = JavaSQLContextSingleton.getInstance(v1.context());
//				sqlContext.setConf(options);
//				org.apache.spark.sql.hive.HiveContext hivContext = 
//						(HiveContext) JavaSQLContextSingleton.getInstanceH(v1.context());

				JavaRDD<WordCountx> rowRDD = v1.map(new Function<Tuple2<String,Integer>, WordCountx>() {

					@Override
					public WordCountx call(Tuple2<String, Integer> v1) throws Exception {
						WordCountx tC = new WordCountx(v1._2(), v1._1());
			            return tC;
					}
					
				});
				
				
				org.apache.spark.sql.DataFrame wordsDataFrame = 
						sqlContext.createDataFrame(rowRDD, WordCountx.class);
				//wordsDataFrame.
				//wordsDataFrame.write().mode(SaveMode.Overwrite);
//				wordsDataFrame.write().options(options);
//				Row r= wordsDataFrame.first();

				java.sql.Connection cn = org.apache.spark
				.sql.execution
				.datasources.jdbc.JdbcUtils.createConnection("jdbc:postgresql://localhost:32768/postgres",
						options);
				
				
//				org.apache.spark
//					.sql.execution
//					.datasources.jdbc.JdbcUtils.saveTable(wordsDataFrame,
//							"jdbc:postgresql://localhost:32768/postgres",
//							"wordcountx", options);

				try{
					WordCountx data = rowRDD.first();
					rowRDD.collect();
					//System.out.println("WD: " + wordsDataFrame.first().mkString());
					StructType fg = wordsDataFrame.schema();
					PreparedStatement ps = org.apache.spark
					  .sql.execution
					  .datasources.jdbc.JdbcUtils.insertStatement
									(cn, "public.wordcountx", fg);
					
					ps.setString(1, data.getWord());
					ps.setInt(2, data.getCounts());
					ps.executeUpdate();
					//cn.createStatement().execute(sql);
					cn.close();
				}catch (Exception ex){
					ex.printStackTrace();
				}
//				org.apache.spark.sql.execution.datasources
//					.jdbc.JdbcUtils.insertStatement(arg0, arg1, type);
				
				/**
				 * This works.
				 */
//				wordsDataFrame.write().jdbc(
//						"jdbc:postgresql://localhost:32768/postgres",
//						"testcounts", options); 
				
				//wordsDataFrame.write().insertInto("testcounts");
				//sqlContext.executeSql("Insert into ``testcounts`` (Word, count) values('test',100)");
				
//				org.apache.spark.sql.DataFrame wordCountsDataFrame =
//				            sqlContext.sql("select word, count(*) as total from public.testcounts group by word");
//				wordCountsDataFrame.show();
			}
			
		
		});
		
		
		
		wordCounts.print();
		//System.out.println("woa" + wordCounts.);
		jssc.start();
		jssc.awaitTermination();
	}
	
	
	static class JavaSQLContextSingleton {
		  private static transient SQLContext instance = null;
		  public static SQLContext getInstance(SparkContext sparkContext) {
		    if (instance == null) {
		      instance = new SQLContext(sparkContext);
		    }
		    return instance;
		  }
		}
}
